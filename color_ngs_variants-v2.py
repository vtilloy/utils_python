#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Add color to NGS variants csv file (from aspicov/aspicmv/microbiomic) according to allelic frequency and presence into a reference.txt file
# Version2

import sys
import pandas as pd
import openpyxl
import json
import glob
import re

def process_csv(csv_file):
    try:
        with open(csv_file, 'r') as f:
            first_line = f.readline().strip().split('\t')
            num_fields = min(15, len(first_line))
            
            lines = [first_line] + [line.strip().split('\t')[:num_fields] for line in f.readlines()]
            
            df = pd.DataFrame(lines[1:], columns=lines[0])

        print(f"Successfully processed {csv_file}")

        output_file = csv_file[:-4] + "-final.xlsx"
        
        with pd.ExcelWriter(output_file, engine='openpyxl') as writer:
            df.to_excel(writer, sheet_name='Results', index=False)
                        
            legend_data = {
                'green': {
                    'Description': "coding change <= 2% (non-silent and allelic frequency <= 0.002)",
                    'Color': 'C1FFC1'
                },
                'yellow': {
                    'Description': "coding change 2 to 20% (non-silent and allelic frequency between 0.002 and 0.02): not detected by Sanger sequencing",
                    'Color': 'FFFFE0'
                },
                'orange': {
                    'Description': "coding change 20% to 100% (if non-silent and allelic frequency superior to 0.02): detected by Sanger and Proton NGS",
                    'Color': 'FFDAB9'
                },
                'red': {
                    'Description': "coding change conferring antiviral resistance but not detected by Sanger (in BDD-v11 list and with allelic frequency below 0.02)",
                    'Color': 'FFB6C1'
                },
                'other red': {
                    'Description': "coding change conferring antiviral resistance and detected by Sanger (in BDD-v11 list and with allelic frequency above 0.02)",
                    'Color': 'FF6347'
                },
                'blue': {
                    'Description': "deletion",
                    'Color': 'ADD8E6'
                },
                'other blue': {
                    'Description': "codon Stop",
                    'Color': '778899'
                }
            }
            legend_df = pd.DataFrame.from_dict(legend_data, orient='index', columns=['Description', 'Color'])
            legend_sheet = writer.book.create_sheet(title='Legend')
            for idx, row in enumerate(legend_df.iterrows(), start=1):
                color_code = row[1]['Color']
                description = row[1]['Description']

                legend_sheet.cell(row=idx, column=1).value = description
                legend_sheet.cell(row=idx, column=1).fill = openpyxl.styles.PatternFill(start_color=color_code,
                                                                                          end_color=color_code,
                                                                                          fill_type='solid')
            worksheet = writer.sheets['Results']
            for row_idx, row in enumerate(df.values, start=2):
                amino_acid_mutation = row[8]
                gene = row[7]
                try:
                    allelic_frequency = float(row[6])
                except:
                    allelic_frequency = 0  
                                  
#                print(f"Row {row_idx}: {row}")
#                print(f"Gene in row {row_idx}: {gene}")

                row_color = None
                if any((gene == m_entry[1] and amino_acid_mutation in m_entry[0]) for m_entry in mutation_list):
                    if allelic_frequency:
                        try:
                            frequency = float(allelic_frequency)
                            if frequency <= 0.002:
                                row_color = 'C1FFC1'
                            elif 0.002 < frequency <= 0.02:
                                row_color = 'FFFFE0'
                            elif 0.02 < frequency <= 1:
                                row_color = 'FFDAB9'
                        except ValueError:
                            print(f"Invalid allelic frequency in row {row_idx}: {allelic_frequency}")
                else:
                    if re.match(r'^[A-Za-z]\d+[A-Za-z]$', amino_acid_mutation):
                        row_color = 'ebfbfa'
                    elif 'Stop' in amino_acid_mutation:
                        row_color = 'ADD8E6'
                    elif 'del' in amino_acid_mutation:
                        row_color = '778899'

                if row_color:
                    row_cells = worksheet.iter_rows(min_row=row_idx, max_row=row_idx, min_col=1, max_col=len(row))
                    for cell in row_cells:
                        for c in cell:
                            c.fill = openpyxl.styles.PatternFill(start_color=row_color,
                                                                  end_color=row_color, fill_type='solid')

                for col_idx, value in enumerate(row, start=1):
                    cell = worksheet.cell(row=row_idx, column=col_idx)
                    cell.value = value
                    
                    if col_idx in [2, 5, 6, 7]:
                        try:
                            value = float(value)
                        except ValueError:
                            print(f"Non-numeric value in row {row_idx}, column {col_idx}: {value}")

        print(f"Saved processed data to {output_file}")
        
    except Exception as e:
        print(f"Error processing CSV file {csv_file}: {e}")

if len(sys.argv) < 2:
    print("Usage: python script.py <csv_file1> [<csv_file2> ...] or use * to specify multiple files")
    sys.exit(1)

with open('mutations-resistance-BDD-v11.txt') as file:
    mutation_list = json.load(file)

csv_files = []
for arg in sys.argv[1:]:
    csv_files.extend(glob.glob(arg))

for csv_file in csv_files:
    process_csv(csv_file)