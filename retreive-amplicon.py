#!/usr/bin/python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: retreive amplicons from bedpe file (right/left primers)

def parse_amplicon_file(amplicon_file):
    primer_info = {}
    with open(amplicon_file, 'r') as amplicons:
        for line in amplicons:
            fields = line.strip().split('\t')
            chrom = fields[0]
            start = int(fields[1])
            end = int(fields[2])
            primer_name = fields[3]
            primer_info[primer_name] = (chrom, start, end)
    return primer_info

def generate_amplicons(primer_info):
    amplicons = []
    paired_primers = set()

    for primer_name, (chrom, start, end) in primer_info.items():
        if primer_name.endswith("_LEFT"):
            right_primer_name = primer_name.replace("_LEFT", "_RIGHT")
            if right_primer_name in primer_info:
                right_chrom, right_start, right_end = primer_info[right_primer_name]
                if chrom == right_chrom:
                    amplicons.append((chrom, min(start, right_start), max(end, right_end)))
                    paired_primers.add(primer_name)
                    paired_primers.add(right_primer_name)

    for primer_name in paired_primers:
        del primer_info[primer_name]

    return amplicons

amplicon_file = 'artic_v4-1_ncov-2019-primer.txt'
primer_info = parse_amplicon_file(amplicon_file)
amplicons = generate_amplicons(primer_info)

for amplicon in amplicons:
    print(f'Amplicon {amplicon[0]}:{amplicon[1]}-{amplicon[2]}')

