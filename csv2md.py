#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Convert a csv file into a markdown table

import pandas as pd

# Load the CSV file
df = pd.read_csv('sars_cov2_variants.csv')

# Convert to markdown
markdown_table = df.to_markdown(index=False)

# Or to save directly to a file:
with open("README.md", "a") as f:
    f.write("\n" + markdown_table + "\n")