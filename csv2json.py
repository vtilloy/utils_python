#!/usr/bin/env python3 

# from https://gitlab.com/vtilloy/utils_python 
# Purpose of the script: convert csv data from file into json file 

import pandas as pd

file_path = '/path/table.csv'
df = pd.read_csv(file_path, sep='\t')

json_output = df.to_json(orient='records', indent=2)

output_file = 'results.json'
with open(output_file, 'w') as file:
    file.write(json_output)

print(json_output)