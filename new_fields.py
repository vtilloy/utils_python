#!/usr/bin/env python3 

# from https://gitlab.com/vtilloy/utils_python 
# Purpose of the script: Add fields technology with Sanger and field glims without data to json sequence file 

import json

with open('BMG_extracted_renamed_fasta_no_spaces_20241009_4.json', 'r') as file:
    data = json.load(file)

for entry in data:
    entry['technology'] = "Sanger"
    entry['glims'] = ""

with open('BMG_extracted_newfields.json', 'w') as file:
    json.dump(data, file, indent=4)