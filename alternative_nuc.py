#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Replace nucleotide by IUPAC ambiguity base in VCF when heterozygous

import sys
import vcf

def get_ambiguity_code(ref, alts):
    """
    IUPAC ambiguity codes for nucleotide bases:
    """
    amb_code_map = {
        ('A', 'G'): 'R',
        ('G', 'A'): 'R',    
        ('C', 'T'): 'Y',
        ('T', 'C'): 'Y',
        ('A', 'C'): 'M',
        ('C', 'A'): 'M',
        ('G', 'T'): 'K',
        ('T', 'G'): 'K',
        ('C', 'G'): 'S',
        ('G', 'C'): 'S',
        ('G', 'T'): 'K',
        ('T', 'G'): 'K',             
        ('A', 'T'): 'W',
        ('T', 'A'): 'W'
#        ('A', 'C', 'T'): 'H',
#        ('C', 'G', 'T'): 'B',
#        ('A', 'C', 'G'): 'V',
#        ('A', 'G', 'T'): 'D'
    }

    for alt in alts:
        if (ref, alt) in amb_code_map:
            return amb_code_map[(ref, alt)]
    return 'N'

def preprocess_vcf(input_vcf, output_vcf):

    vcf_reader = vcf.Reader(open(input_vcf, 'r'))
    vcf_writer = vcf.Writer(open(output_vcf, 'w'), vcf_reader)

    for record in vcf_reader:
        ref = record.REF
        alts = [str(alt) for alt in record.ALT]
        
        if record.num_het > 0:
            amb_code = get_ambiguity_code(ref, alts)
            record.ALT = [vcf.model._Substitution(amb_code)]
        
        if len(ref) > 1 or any(len(alt) > 1 for alt in alts):
            record.REF = 'N'
            record.ALT = [vcf.model._Substitution('N')]
        
        vcf_writer.write_record(record)

    vcf_writer.close()

def main():
    if len(sys.argv) != 3:
        print("Usage: python script.py <input.vcf> <output.vcf>")
        sys.exit(1)

    input_vcf = sys.argv[1]
    output_vcf = sys.argv[2]
    
    preprocess_vcf(input_vcf, output_vcf)
    print("Preprocessing complete. Output written to", output_vcf)

if __name__ == "__main__":
    main()