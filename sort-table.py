#!/usr/bin/python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Sort table of genes+counts before PCA

import pandas as pd

# Read table into a pandas dataframe
df = pd.read_csv("test.csv", sep="\t")

# Generate a dictionary to map gene names to their corresponding index
gene_indices = {}
for i, gene in enumerate(df["gene"]):
    if gene not in gene_indices:
        gene_indices[gene] = []
    gene_indices[gene].append(i)

# Generate a new dataframe with the sorted rows
sorted_rows = []
for gene, indices in gene_indices.items():
    for i in indices:
        sorted_rows.append(df.iloc[i])
df_sorted = pd.DataFrame(sorted_rows)

# write the sorted table to a new file
df_sorted.to_csv("sorted_table.csv", sep="\t", index=False)